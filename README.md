# Porth VSCode Extension

A language grammar extension for syntax highlighting in VSCode for Porth Programming Language.

To install, put this directory in `$home/.vscode/extensions`. On my linux system it's `~/.vscode/extensions`. Reload VSCode and it should work!

Porth Programming Language: https://gitlab.com/tsoding/porth